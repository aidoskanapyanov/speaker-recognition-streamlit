import time, os
import logging
import streamlit as st
import numpy as np
import librosa, librosa.display
import matplotlib.pyplot as plt
from PIL import Image
from settings import IMAGE_DIR, DURATION, WAVE_OUTPUT_FILE, RECORDING_DIR
from src.sound import sound
from src.model import CNN
from setup_logging import setup_logging
import warnings
from os import listdir, name
from os.path import isfile, join
import pandas as pd

# from IPython.display import Audio
# from ipywebrtc import CameraStream, AudioRecorder

from speechbrain.pretrained import SpeakerRecognition, EncoderClassifier
verification = SpeakerRecognition.from_hparams(source="speechbrain/spkrec-ecapa-voxceleb", savedir="pretrained_models/spkrec-ecapa-voxceleb")
import torchaudio
import torch
from scipy import spatial
classifier = EncoderClassifier.from_hparams(source="speechbrain/spkrec-xvect-voxceleb", savedir="pretrained_models/spkrec-xvect-voxceleb")

setup_logging()
logger = logging.getLogger('app')

def main():
    global new_recording
    title = "Speaker Verification"
    st.title(title)
    image = Image.open(os.path.join(IMAGE_DIR, 'vv.jpg'))
    st.image(image, width=400)

    if st.button('Record first voice'):
        with st.spinner(f'Recording for {DURATION} seconds ....'):
            sound.record(1)
        st.success("Recording completed")


    if st.button('Play first voice'):
        sound.play(1)
        try:
            audio_file = open(WAVE_OUTPUT_FILE[:-4]+'1.wav', 'rb')
            audio_bytes = audio_file.read()
            st.audio(audio_bytes, format='audio/wav')
        except:
            st.write("Please record sound first")
    
    if st.button('Record second voice'):
        with st.spinner(f'Recording for {DURATION} seconds ....'):
            sound.record(2)
        st.success("Recording completed")

    if st.button('Play second voice'):
        sound.play(2)
        try:
            audio_file = open(WAVE_OUTPUT_FILE[:-4]+'2.wav', 'rb')
            audio_bytes = audio_file.read()
            st.audio(audio_bytes, format='audio/wav')
        except:
            st.write("Please record sound first")

    if st.button('Verify'):
        score, prediction = verification.verify_files(WAVE_OUTPUT_FILE[:-4]+'1.wav', WAVE_OUTPUT_FILE[:-4]+'2.wav')
        st.success("Comparison completed")
        st.write(f'### The score is {score[0]} and the prediction is {prediction[0]}')
        st.write("\n")

    placeholder = st.empty()

    name = placeholder.text_input('You can add new person here')
    click_clear = st.button('Clear text input', key=1)
    if click_clear:
        name = placeholder.text_input('You can add new person here', value='', key=1)
    
    if name:
        st.success("New person is created")
    # while st.button('Add new subject to database') == True:
    
    # name = st.text_input('New subject\'s name')
    # # breakpoint()
    # if not name:
    #     st.warning('Please input a name.')
    #     st.stop()
    #         # raise st.ScriptRunner.StopException
    # st.success("New subject is created")
        
        # print(name)
        # print(type(name))

        # raise st.ScriptRunner.RerunException
        # st.rerun()
        # with st.spinner(f'Recording for {DURATION} seconds ....'):
        #     sound.record(3, '')
    #     new_recording = st.text_input("What is your name?")

    if st.button('Record new subject\'s voice'):    
        with st.spinner(f'Recording for {DURATION} seconds ....'):
            sound.record(3, name)
        st.success("Recording completed")
        
    if st.button('Identify'):
        mypath = RECORDING_DIR + '/dataset2/'
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        
        # signal, fs = torchaudio.load(WAVE_OUTPUT_FILE[:-4]+'1.wav')
        # embeddings1 = classifier.encode_batch(signal)
        scores = {}
        for x, y in [ [mypath+'/'+x, x[:-4]] for x in onlyfiles]:
            # signal2, fs = torchaudio.load(x)
            # embeddings2 = classifier.encode_batch(signal2)
            # cos = torch.nn.CosineSimilarity(dim=1, eps=1e-6)
            # score2 = cos(embeddings1[0], embeddings2[0])

            # score2 = 1 - spatial.distance.cosine(embeddings1.tolist()[0][0], embeddings2.tolist()[0][0])
            

            score, prediction = verification.verify_files(WAVE_OUTPUT_FILE[:-4]+'1.wav', x)
            # scores[y] = score2#.tolist()[0]
            scores[y] = score.tolist()[0]
        
        # st.write(embeddings1[0])
        # st.write(embeddings2[0])
        # st.write(score2)
        st.write(f'Comparison results:')
        st.write('\n')
        # st.write(f'{scores}')

        my_df = pd.DataFrame(scores.items(), columns=['Names', 'Cosine sim']).sort_values(by=['Cosine sim'], ascending=True)

        # st.dataframe(my_df)
        
        st.set_option('deprecation.showPyplotGlobalUse', False)
        my_df.plot.barh(x='Names', y='Cosine sim')
        plt.show()
        st.pyplot()

        st.write('\n')
        st.write(f'The person is: {max(scores, key=scores.get).capitalize()}')

    
    # if st.button('asd'):
    #     camera = CameraStream(constraints={'audio': True,
    #                                 'video': False},
    #                     mimeType='audio/wav')
    #     recorder = AudioRecorder(stream=camera)
    #     recorder.recording = True
    #     from time import sleep
    #     sleep(2.5)
    #     recorder.recording = False
    #     recorder.save('asdasdasd.wav')
    #     st.write('\n')


    

if __name__ == '__main__':
    main()